const { exec } = require('child_process')
const http = require('http')

const hostname = "127.0.0.1"
const port = 3000

const server = http.createServer((req, res) => {
    if (req.url == '/live') {
        exec('./server.sh', (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`)
                //res.end(`error: ${error.message}`)
                return
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`)
                //res.end(`stderr: ${stderr}`)
                return
            }
        })
        console.log('video stream started')
        res.end('video stream started')
        return
    }
    res.end()
})

server.listen(port, hostname, () => console.log(`Server running at http://${hostname}:${port}/`))
